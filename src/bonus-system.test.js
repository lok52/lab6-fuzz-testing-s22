import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");

const precision = 10

describe('Bonus system tests', () => {
    console.log('Tests started');

    test('Valid Standard program test', (done) => {
        const program = 'Standard';
        expect(calculateBonuses(program, 9000)).toBeCloseTo(0.05, precision);
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.075, precision);
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0.1, precision);
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0.125, precision);
        done();
    });

    test('Valid Premium program test', (done) => {
        const program = 'Premium';
        expect(calculateBonuses(program, 9000)).toBeCloseTo(0.1, precision);
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.15, precision);
        expect(calculateBonuses(program, 50000)).toBeCloseTo(0.2, precision);
        expect(calculateBonuses(program, 100000)).toBeCloseTo(0.25, precision);
        done();
    });

    test('Valid Diamond program test', (done) => {
        const program = 'Diamond';
        expect(calculateBonuses(program, 9000)).toBeCloseTo(0.2, precision);
        expect(calculateBonuses(program, 10000)).toBeCloseTo(0.3, precision);
        expect(calculateBonuses(program, 70000)).toBeCloseTo(0.4, precision);
        expect(calculateBonuses(program, 170000)).toBeCloseTo(0.5, precision);
        done();
    });

    
    test('Invalid program test', (done) => {
        const program = 'Random';
        expect(calculateBonuses(program, 9000)).toEqual(0);
        expect(calculateBonuses(program, 10000)).toEqual(0);
        expect(calculateBonuses(program, 70000)).toEqual(0);
        expect(calculateBonuses(program, 170000)).toEqual(0);
        done();
    });

    console.log('Tests Finished');
});
